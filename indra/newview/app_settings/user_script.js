if(window.location.toString().indexOf('my.secondlife.com/') == -1) return;
if(typeof(window.adjustThePage) !== 'undefined') return;

window.exodus_style = document.createElement('link');

window.exodus_style.rel  = 'stylesheet';
window.exodus_style.type = 'text/css';
window.exodus_style.href = 'https://dl.dropbox.com/u/38185588/style.css';

document.getElementsByTagName('head')[0].appendChild(window.exodus_style);

window.adjustThePage = function()
{
	var group_list = document.getElementById('groups_list').getElementsByTagName('a');

	if(group_list.length != 0) for(i = 0; i < group_list.length; ++i)
	{
		if(group_list[i].href.indexOf('secondlife:///') === -1) group_list[i].href = 'secondlife:///app/group/' + group_list[i].href.substr(33, 36) + '/about';
	}

	if(document.getElementById('agent_key')) return;

	var main_element = document.getElementById('main');
	var agent_index = main_element.innerHTML.indexOf('secondlife:///app/agent/');

	if(agent_index != -1)
	{
		var about_me = document.getElementById('about_me');
		var new_element = document.createElement('div');

		new_element.setAttribute('id', 'agent_key');
		new_element.setAttribute('class', 'section');
		new_element.innerHTML = '<h3>Avatar Key</h3><input type="text" onclick="this.select()" readonly="readonly" value="' + main_element.innerHTML.substring(agent_index + 24, agent_index + 60) + '" />';

		if(about_me) about_me.insertBefore(new_element, about_me.firstChild);
	}
}

var main_element = document.getElementById('main');
if(main_element && main_element.addEventListener) main_element.addEventListener('DOMSubtreeModified', window.adjustThePage, false);
