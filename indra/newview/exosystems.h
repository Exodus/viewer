/** 
 * @file exosystems.h
 * @brief exoSystems class definitions
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011, Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_SYSTEMS
#define EXO_SYSTEMS

#include "llsingleton.h"
#include "lleventtimer.h"

class exoSystems :
	public LLSingleton<exoSystems>,
	public LLEventTimer
{
public:
	exoSystems();

	static std::string loginMessage;

	//static std::string updateTitle;
	//static std::string updateMessage;
	//static std::string updateURL;
	
	static std::string databaseBackendLocation;
	static std::string databaseBackendLabel;
	static std::string databaseBackendSalt;

	//static bool isBlacklisted;

	static bool hasLoginMessage;

	static bool disableSpacebar;

	static KEY mRTAKey;
	static KEY mRTDKey;

	static BOOL keyDown(KEY key, MASK mask, BOOL repeated);
	static BOOL keyUp(KEY key, MASK mask);

	static void getData();
	static void useLocalData();
	static void useReleaseData();
	static void phraseData(LLSD data);
	static void updateSettings();
	
	static void handleTeleport(U32 type = 0);
	
	static LLUUID getRezGroup();
	
	void smashDrawDistance();

	BOOL getStarted();

private:
	F32 drawDistance;
	
	virtual BOOL tick();
};

#endif // EXO_SYSTEMS
