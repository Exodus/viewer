/**
 * @file exonotifier.h
 * @brief Defines the interface for OS-specific notifiers
 *
 * $LicenseInfo:firstyear=2012&license=viewerlgpl$
 * Copyright (C) 2012 Katharine Berry
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */
#ifndef EXONOTIFIER_H
#define EXONOTIFIER_H

#include <string>

class exoNotifier
{
public:
	virtual void showNotification(const std::string& notification_title, const std::string& notification_message, const std::string& notification_type) { }
	virtual bool isUsable() { return false; }
	virtual void registerApplication(const std::string& application, const std::set<std::string>& notificationTypes) { }
	virtual bool needsThrottle() { return true; }
	virtual void clearDelivered() { }
};

#endif // EXONOTIFIER_H